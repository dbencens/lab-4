//
//  BlockViewController.swift
//  Lab-4
//
//  Created by Daniel Bencens on 23/10/2014.
//  Copyright (c) 2014 Daniel Bencens. All rights reserved.
//

import UIKit

class BlockViewController: UITableViewController, UISearchBarDelegate, UISearchDisplayDelegate {

    
    
    @IBOutlet weak var searchBar: UISearchBar!
    var bookArray = [BookSummary]()
   
    
    
    override func  viewDidLoad() {
        super.viewDidLoad()
        //search
        self.searchBar.delegate = self
        self.refeshData("")
    }
  
    
   func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
       println(searchText)
   
   self.refeshData(searchText)
  
    }
    
    
    func refeshData(searchEntry: String) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        Booklist.getBooks({(books: Array<BookSummary>)in
            for books in books {
                println(books.title)
            }
            self.bookArray = books
            dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            })
    },searchEntry: searchEntry)
    }
    
   
    
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
      
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       //search
        
        
            return self.bookArray.count
        
    
        }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BookItem", forIndexPath: indexPath) as UITableViewCell
        if bookArray.count > 0 {
            let bookSummary = bookArray[indexPath.row]
            cell.textLabel.text = bookSummary.title
            cell.detailTextLabel?.text = bookSummary.subTitle
            if let url = NSURL(string: bookSummary.thumbnail) {
                if let data = NSData(contentsOfURL: url) {
                    if let image:UIImage = UIImage(data: data) {
                        cell.imageView.image = image
                    }
                }
            }
        }
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
